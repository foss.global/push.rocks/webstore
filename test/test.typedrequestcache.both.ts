import { expect, tap } from '@pushrocks/tapbundle';
import * as webstore from '../ts/index.js';

let testTypedrequestcache: webstore.TypedrequestCache;

tap.test('first test', async () => {
  testTypedrequestcache = new webstore.TypedrequestCache('https://test.lossless.com/typedrequest');
  expect(testTypedrequestcache).toBeInstanceOf(webstore.TypedrequestCache);
});

tap.start();
