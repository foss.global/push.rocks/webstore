import { expect, tap } from '@pushrocks/tapbundle';
import * as webstore from '../ts/index.js';

let testWebstore: webstore.WebStore;

tap.test('first test', async () => {
  testWebstore = new webstore.WebStore({
    dbName: 'mytest-db',
    storeName: 'mytest-store',
  });
  expect(testWebstore).toBeInstanceOf(webstore.WebStore);
});

tap.test('should allow storing a string', async () => {
  await testWebstore.set('mystring', 'heythere');
  expect(await testWebstore.get('mystring')).toEqual('heythere');
});

tap.test('should allow storing an object', async () => {
  await testWebstore.set('testProp1', {
    wow: 'wowVal',
  });
  console.log(JSON.stringify(await testWebstore.get('testProp1')));
});

tap.test('should overwrite a value', async () => {
  await testWebstore.set('testProp1', {
    wow: 'wowVal2',
  });
  console.log(JSON.stringify(await testWebstore.get('testProp1')));
});

tap.test('should correctly check the existence of keys', async () => {
  const resultNotThere = await testWebstore.check('notThere');
  const resultThere = await testWebstore.check('testProp1');
  expect(resultNotThere).toBeFalse();
  expect(resultThere).toBeTrue();
});

tap.start();
