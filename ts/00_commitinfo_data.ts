/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/webstore',
  version: '2.0.8',
  description: 'high performance storage in the browser using indexed db'
}
